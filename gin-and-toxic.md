# Gin and Toxic

Tap. Tap. 

I could hear the sound. It was slightly muffled. Where was it coming from?

Tap. Tap.

There it was again. Louder.

Tap, Tap, Tap,

"Hey, Mack. Either you roll down this window or I'm going to break it."

I opened my eyes. I could see the outline of a large policeman in the early sunlight outside my window, holding a battered nightstick.

I adjusted my glasses, as I groped for the window handle. Finding it, I cranked the window down halfway.

"Morning, officer. What can I do for you?"

"You can explain why you're out here asleep in your car, instead of tucked in at home."

I rubbed the sleep out of my eyes as I spoke. "Wife and I had a fight last night. She kicked me out for the night. I decided to sleep in the car, instead of going to a hotel."

"Which building?"

"What?"

"Which building did she kick you out of?"

I gestured over my shoulder. "That one. We haven't lived in it long. Just moved here from Morrisburg."

The cop didn't bother looking in the direction I'd pointed out. "I doubt it. No one lives there. It's an office building. Been that way for five years."

"Oh. I guess I should have know that."

The cop leaned forward and took a closer look at me. "Hey, I know you. I've seen you around the station. You're that private detective that the captain keeps complaining about. Lungduck, right?"

I raised my eyebrows, "That sounds like something you get from a bad restaurant in Chinatown. The name is Luke Dunn."

"I dunno. You look like a duck. What you doing here?"

I yawned. "Company hired me to check up on one of their executives. Looks like I missed him getting in this morning."

"That's nice. I don't like private detectives. You guys make life difficult for us. If I catch you down here again, I'll run you in. Now get."

I nodded. "You got it, Officer." I ground the starter and drove into the growing traffic.

Twenty minutes later, I was climbing the stairs to my third floor office. The elevator had been out for 6 weeks. Five weeks ago, the building manager took the maintenance budget and his secretary to Maui.

When I opened the door to the third floor, there were two people standing outside my door. Thankfully, they looked more like customers than bill collectors. After all, bill collectors don't send out teams of his and hers agents. They send out two hundred pound leg breakers named Tony or Mikey.

The male of the pair was checking his watch when I walked up. 

"Excuse me," I said. "I need to clean this office."

They stepped away from the door. I unlocked the door with my key and stepped in, closing the door behind me.

I tossed the mail and my hat on the desk. I opened a drawer and pulled out a pillow and blanket. I was starting to stretch out on the couch, when there was a knock at the door.

"Mr. Dunn?"

"He won't be in until Monday," I shouted. ""